package com.psa.dashBoard3.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.psa.dashBoard3.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
