import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { Employee } from './employee';
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

private baseURL="http://localhost:8080/api/v1/list";
private baseURL2="http://localhost:8080/api/v1/getById";
private baseURL3="http://localhost:8080/api/v1/employees";
  constructor(private httpClient:HttpClient) { }

  getEmployeesList():Observable<Employee[]>{
    return this.httpClient.get<Employee[]>(`${this.baseURL}`);
  }

  getEmployeesById(Id:number):Observable<Employee>{
    return this.httpClient.get<Employee>(`${this.baseURL2}/${Id}`);
  }

  createEmployee(employee:Employee): Observable<object>{
    return this.httpClient.post(`${this.baseURL3}`,employee);
  }

  updateEmployee(id:number,employee:Employee):Observable<Object>{
    return this.httpClient.put(`${this.baseURL3}/${id}`,employee);
  }
  deleteEmployee(id:number):Observable<Object>{
    return this.httpClient.delete(`${this.baseURL3}/${id}`);
  }

}

