package com.psa.dashBoard3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DashBoard33Application {

	public static void main(String[] args) {
		SpringApplication.run(DashBoard33Application.class, args);
	}

}
