package com.psa.dashBoard3.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.psa.dashBoard3.Repository.EmployeeRepository;
import com.psa.dashBoard3.entity.Employee;

@RestController
@CrossOrigin( origins="http://localhost:4200")
@RequestMapping("api/v1")
public class EmployeeController {
	
	@Autowired
	private EmployeeRepository EmployeeRepo;
	 
	@GetMapping(path = "/list")
	public List<Employee> getAllEmployee(){
		return EmployeeRepo.findAll();
	}
	//get employee by id
	@GetMapping("/getById/{id}")
 public Employee getEmployeeById(@PathVariable("id")Long id) {
	 Optional<Employee> findById = EmployeeRepo.findById(id);
	 Employee employee = findById.get();
	return employee;
 }
	
	//create employee rest api
	@PostMapping("/employees")
	public Employee createEmployee(@RequestBody Employee employee) {
		 return EmployeeRepo.save(employee);
		 
	}
	
	//update employee rest api
	@PutMapping("/employees/{id}")
	public ResponseEntity<Employee> updatEmployee(@PathVariable("id") Long id,@RequestBody Employee employeeDetails){
		Optional<Employee> findById = EmployeeRepo.findById(id);
		Employee employee = findById.get();
		employee.setFirstName(employeeDetails.getFirstName());
		employee.setLastName(employeeDetails.getLastName());
		employee.setEmail(employeeDetails.getEmail());
		Employee updatedEmployee = EmployeeRepo.save(employee);
		return ResponseEntity.ok(updatedEmployee);
		
	}
	
	//delete employee restapi
	@DeleteMapping("/employees/{id}")
	public ResponseEntity<Map<String, Boolean>> deleteEmployee(@PathVariable("id") Long id){
		Optional<Employee> findById = EmployeeRepo.findById(id);
		Employee employee = findById.get();
		EmployeeRepo.delete(employee);
		Map<String,Boolean> response =new HashMap();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
		
	}
	
	
}
