import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Employee} from '../employee'
import { EmployeeService } from '../employee.service';


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employees: Employee[]=[];

  constructor(private employeeService:EmployeeService,
    private router:Router) { }
  employee:Employee=new Employee();
  id!:number;
  firstName!:string;
  lastName!:string;
  

  

  ngOnInit(): void {
    this.getEmployeeById();
   this.employeeService.getEmployeesList().subscribe(data => {
     this.employees=data;
     console.log(this.employees);
   });
  }

  getEmployeeById(){
    this.id=1;
    this.employeeService.getEmployeesById(this.id).subscribe(data =>{
      this.employee=data;
      console.log(this.employee);
     
    })
  }
  employeeDetails(id:number){
    this.router.navigate(['employee-details',id]);
  }

  updateEmployee(id:number){
    this.router.navigate(['update-employee',id]);
  }
  deleteEmployee(id:number){
    this.employeeService.deleteEmployee(id).subscribe(data => {
      console.log(data);
      this.getEmployeeById();
    })
  }
}
